## Name

Weather App

## Description

Created my first weather application using JS, HTML and CSS.

## Badges

N/A

## Visuals

N/A

## Installation

N/A

## Usage

N/A

## Support

Email.

## Roadmap

Finished work.

## Contributing

Open to contributions

## Authors and acknowledgment

Toshiana Williams

## License

N/A

## Project status

Finished May 16, 2023 7:30pm CST
